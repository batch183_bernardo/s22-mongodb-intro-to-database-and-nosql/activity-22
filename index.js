


// USERS Collection
{
	"_id" : "user01",
	"firstName" : "John",
	"lastName" : "Bernardo",
	"email" : "jbernardo@mail.com",
	"password" : "jbernardo",
	"isAdmin" : true,
	"mobileNumber"  : "09987654321",
	"dateTimeRegistered" : "2022-06-10T15:00:00.00z"
}


{
	"_id" : "user02",
	"firstName" : "Juan",
	"lastName" : "Dela Cruz",
	"email" : "jdelacruz@mail.com",
	"password" : "jdelacruz",
	"isAdmin" : false,
	"mobileNumber"  : "09123456789",
	"dateTimeRegistered" : "2022-06-10T15:00:00.00z"
}

//Products

{
	"_id" : "product01",
	"name" : "theGreenery",
	"description" : "cotton",
	"price" : "700",
	"stocks" : "15",
	"isActive" : true,
	"SKU"  : "052322-SMMLXL",
	"dateTimeCreated" : "2022-06-10T15:00:00.00z"
}

{
	"_id" : "product02",
	"name" : "Evil Genius",
	"description" : "silk",
	"price" : "650",
	"stocks" : "10",
	"isActive" : true,
	"SKU"  : "052322-SMMLXL",
	"dateTimeCreated" : "2022-06-10T15:00:00.00z"
}

// Orders Products

{
	"_id" : "OProducts01",
	"orderID" : "Orders01"
	"productID" : "product01",
	"quantity" : 5,
	"price" : 700,
	"subTotal"	: 3500, 
	"dateTimeCreated" : "2022-06-10T15:00:00.00z"
}

{
	"_id" : "OProducts02",
	"orderID" : "Orders01"
	"productID" : "product02",
	"quantity" : 10,
	"price" : 650,
	"subTotal"	: 6500, 
	"dateTimeCreated" : "2022-06-10T15:00:00.00z"
}

// Orders

{
	"_id" : "Orders01",
	"userId" : "user02",
	"transactionDate" : "2022-06-10T15:00:00.00z",
	"status" : "delivered",
	"total" : 10000

}
